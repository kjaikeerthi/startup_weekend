class Admin::DashboardController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"
	require 'csv'
	def index
	end
	def imports
		@customer = Customer.new
		@location = Location.new
		@store = Store.new
		@question = Question.new
	end
	def do_import
		if params[:customer] && params[:customer][:upload_file]
			@file_path = params[:customer][:upload_file].path
		      @upload_customer = Array.new
		      CSV.foreach(@file_path) do |row|
		      	Customer.create({name: row[0], contact_number: row[1], email: row[2]})
		    end
		    redirect_to admin_imports_path
		end
		if params[:location] && params[:location][:upload_file]
			@file_path = params[:location][:upload_file].path
		      @upload_location = Array.new
		      CSV.foreach(@file_path) do |row|
		      	Location.create({country: row[0], state: row[1], city: row[2], zone: row[3], area: row[4]})
		    end
		    redirect_to admin_imports_path
		end

		if params[:store] && params[:store][:upload_file]
			@file_path = params[:store][:upload_file].path
		      @upload_location = Array.new
		      CSV.foreach(@file_path) do |row|
		      	Store.create({name: row[0], unique_key: SecureRandom.hex(4), location_id: row[1]})
		    end
		    redirect_to admin_imports_path
		end

		if params[:question] && params[:question][:upload_file]
			@file_path = params[:question][:upload_file].path
		      @upload_location = Array.new
		      CSV.foreach(@file_path) do |row|
		      	Question.create({question: row[0], store_id: row[1]})
		    end
		    redirect_to admin_imports_path
		end
	end
end
