class Question < ActiveRecord::Base
  belongs_to :store
  has_many :answers
end
