class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :customer
  belongs_to :store
  # has_one :store, through :question
end
