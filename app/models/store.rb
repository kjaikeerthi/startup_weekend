class Store < ActiveRecord::Base
  
  # Friendly ID
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :location
  has_many :questions
end
