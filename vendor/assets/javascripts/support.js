$(function(){
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $("meta[name='csrf-token']").attr("content");
        xhr.setRequestHeader("X-CSRF-Token", token);
    });
})
$.browser={};(function(){jQuery.browser.msie=false;
$.browser.version=0;if(navigator.userAgent.match(/MSIE ([0-9]+)\./)){
$.browser.msie=true;jQuery.browser.version=RegExp.$1;}})();
