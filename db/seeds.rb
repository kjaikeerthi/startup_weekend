# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Admin.destroy_all
Admin.create({first_name: "Jai", last_name: "Keerthi", email: "jai@eatwithus.in", password: "password", gender: "male", contact_number: "9686413729"})

Location.destroy_all

[
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Brigade Road"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Commercial Street"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Cunningham Road"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Infantry Road"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Langford Town"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "Lavelle Road"},
 {country: "india", state: "karnataka", city: "bangalore", zone: "central", area: "MG Road"}
].each do |location|
  Location.create(location)
end 

Store.destroy_all
["Ammies Biriyani","Kubera","Black Perl","Megna","Nandhini Palace"].each do |name|
	Store.create({name: name, unique_key: SecureRandom.hex(4), location_id: Location.last.id})
end

Question.destroy_all
Question.create({question: "What prompted you to order from our restaurant?", store_id: Store.last})
Question.create({question: "How would you rate our Biriyani's taste in terms of taste, aroma and presentation?", store_id: Store.last})
Question.create({question: "Question 3?", store_id: Store.last})
Question.create({question: "Question 4?", store_id: Store.first})

Customer.destroy_all
Customer.create({name: "Rakesh", contact_number: "9842342343", email: "rakesh71187@gmail.com"})
Customer.create({name: "", contact_number: "", email: "rakesh@gmail.com"})
Customer.create({name: "Peter", contact_number: "", email: "rakesh@gmail.com"})
Customer.create({name: "", contact_number: "9683434344", email: "rakesh@gmail.com"})