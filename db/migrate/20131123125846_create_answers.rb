class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :answer
      t.integer :question_id
      t.integer :customer_id
      t.integer :store_id
      t.timestamps
    end
  end
end
