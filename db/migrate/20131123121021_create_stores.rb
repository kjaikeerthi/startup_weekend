class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :slug
      t.string :unique_key
      t.integer :location_id      
      t.timestamps
    end
  end
end
